package ru.rencredit.jschool.kuzyushin.tm.test.repository;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.repository.SessionRepository;

import java.util.List;

public class SessionRepositoryTest {

    private final ISessionRepository sessionRepository = new SessionRepository();

    final private static Session testSession = new Session();

    @BeforeClass
    public static void initUser() {
        testSession.setSignature("sessionOne987654321");
        testSession.setTimestamp(123456789L);
        testSession.setUserId("123123");
    }

    @Before
    public void addProject() {
        sessionRepository.add(testSession);
    }

    @After
    public void deleteProject() {
        sessionRepository.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() throws Exception {
        sessionRepository.add(testSession);
        final Session session = sessionRepository.findSessionByUserId(testSession.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getId(), testSession.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() throws Exception {
        final Session session = sessionRepository.findSessionByUserId(testSession.getUserId());
        Assert.assertNotNull(session);
        sessionRepository.remove(session);
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        Assert.assertEquals(1, sessionRepository.findAllSessions(testSession.getUserId()).size());
        sessionRepository.clear(testSession.getUserId());
        Assert.assertEquals(0, sessionRepository.findAllSessions(testSession.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        final List<Session> sessions = sessionRepository.findAllSessions(testSession.getUserId());
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessions.size(), 1);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() throws Exception {
        final Session session = sessionRepository.findSessionByUserId(testSession.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getId(), testSession.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdTest() throws Exception {
        final Session session = sessionRepository.findSessionByUserId(testSession.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getId(), testSession.getId());
    }

    @Category(UnitCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByUserIdTest() throws Exception {
        final Session session = sessionRepository.findSessionByUserId(testSession.getUserId());
        Assert.assertNotNull(session);
        sessionRepository.removeSessionByUserId(session.getUserId());
        Assert.assertNull(sessionRepository.findSessionById(testSession.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() throws Exception {
        Assert.assertTrue(sessionRepository.contains(testSession.getId()));
    }
}
