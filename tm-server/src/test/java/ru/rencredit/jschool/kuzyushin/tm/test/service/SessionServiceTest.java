package ru.rencredit.jschool.kuzyushin.tm.test.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.bootstrap.Bootstrap;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.repository.SessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;


public class SessionServiceTest {

    final private static IServiceLocator serviceLocator = new Bootstrap();

    final private static ISessionRepository sessionRepository = new SessionRepository();

    final private static ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @BeforeClass
    public static void initData() {
        serviceLocator.getUserService().create("testUser", "123123", "testUser@test.com");
    }

    @Test
    @Category(UnitCategory.class)
    public void openTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        Assert.assertEquals(session, sessionRepository.findSessionById(session.getId()));
        sessionService.close(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void signTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionService.sign(session));
        Assert.assertNotNull(session.getSignature());
        sessionService.close(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void getUserTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserService().findByLogin("testUser");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), sessionService.getUser(session).getId());
        sessionService.close(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void getUserIdTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        final String userId = sessionService.getUserId(session);
        Assert.assertEquals(userId, sessionService.getUser(session).getId());
        sessionService.close(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void getListSessionTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        Assert.assertEquals(1, sessionService.getListSession(session).size());
        sessionService.close(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void closeTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        sessionService.close(session);
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void closeAllTest() throws Exception {
        final Session session1 = sessionService.open("testUser", "123123");
        final Session session2 = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session1);
        Assert.assertNotNull(session2);
        sessionService.closeAll(session1);
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void signOutByLoginTest() throws Exception {
        Assert.assertNotNull(sessionService.open("testUser", "123123"));
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionService.signOutByLogin("testUser");
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void signOutByUserIdTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertEquals(1, sessionRepository.findAll().size());
        sessionService.singOutByUserId(session.getUserId());
        Assert.assertEquals(0, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void checkDataAccessTest() {
        Assert.assertTrue(sessionService.checkDataAccess("testUser", "123123"));
    }

    @Test
    @Category(UnitCategory.class)
    public void isValidTest() throws Exception {
        final Session session = sessionService.open("testUser", "123123");
        Assert.assertNotNull(session);
        Assert.assertTrue(sessionService.isValid(session));
        sessionService.close(session);
    }
}
