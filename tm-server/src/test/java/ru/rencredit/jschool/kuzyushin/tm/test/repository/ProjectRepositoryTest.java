package ru.rencredit.jschool.kuzyushin.tm.test.repository;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectRepositoryTest {

    private final IProjectRepository projectRepository = new ProjectRepository();

    final private static Project testProject = new Project();

    @BeforeClass
    public static void initProject() {
        testProject.setName("projectOne");
        testProject.setUserId("123123");
    }

    @Before
    public void addProject() {
        projectRepository.add(testProject);
    }

    @After
    public void deleteProject() {
        projectRepository.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        projectRepository.add(testProject);
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testProject.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        projectRepository.remove(project);
        Assert.assertEquals(0, projectRepository.findAllProjects(testProject.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        final List<Project> projects = projectRepository.findAllProjects(testProject.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projects.size(), 1);
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        Assert.assertEquals(1, projectRepository.findAllProjects(testProject.getUserId()).size());
        projectRepository.clear(testProject.getUserId());
        Assert.assertEquals(0, projectRepository.findAllProjects(testProject.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testProject.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameTest() {
        final Project project = projectRepository.findProjectByName(testProject.getUserId(), testProject.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testProject.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIndexTest() {
        final Project project = projectRepository.findProjectByIndex(testProject.getUserId(), 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testProject.getName());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByIdTest() {
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        projectRepository.removeProjectById(project.getUserId(), project.getId());
        Assert.assertNull(projectRepository.findProjectById(testProject.getUserId(), testProject.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByIndexTest() {
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        projectRepository.removeProjectByIndex(project.getUserId(), 0);
        Assert.assertNull(projectRepository.findProjectById(testProject.getUserId(), testProject.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByNameTest() {
        final Project project = projectRepository.findProjectById(testProject.getUserId(), testProject.getId());
        Assert.assertNotNull(project);
        projectRepository.removeProjectByName(project.getUserId(), project.getName());
        Assert.assertNull(projectRepository.findProjectById(testProject.getUserId(), testProject.getId()));
    }
}
