package ru.rencredit.jschool.kuzyushin.tm.test.service;

import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.repository.UserRepository;
import ru.rencredit.jschool.kuzyushin.tm.service.UserService;

public class UserServiceTest {

    final private IUserRepository userRepository = new UserRepository();

    final private IUserService userService = new UserService(userRepository);

    final private static User testUser = new User();

    @BeforeClass
    public static void initUser() {
        testUser.setLogin("userOne");
        testUser.setId("123123");
        testUser.setEmail("testUser@test.com");
        testUser.setPasswordHash("password");
        testUser.setFirstName("first-name");
        testUser.setLastName("last-name");
        testUser.setMiddleName("middle-name");
    }

    @Before
    public void addUser() {
        userRepository.add(testUser);
    }

    @After
    public void deleteUser() {
        userRepository.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        final User user = userService.findById(testUser.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(testUser.getId(), user.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLoginTest() {
        final User user = userService.findByLogin(testUser.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(testUser.getId(), user.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeUser(testUser);
        Assert.assertNull(userService.findById(testUser.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByIdTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeById(testUser.getId());
        Assert.assertNull(userService.findById(testUser.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLoginTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeByLogin(testUser.getLogin());
        Assert.assertNull(userService.findById(testUser.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() {
        userService.create(testUser.getId(), testUser.getLogin());
        final User user = userService.findById(testUser.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void createWithEmailTest() {
        userService.create(testUser.getId(), testUser.getLogin(), testUser.getEmail());
        final User user = userService.findById(testUser.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
    }

    @Test
    @Category(UnitCategory.class)
    public void createWithRoleTest() {
        userService.create(testUser.getId(), testUser.getLogin(), Role.USER);
        final User user = userService.findById(testUser.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getRole(), testUser.getRole());
    }

    @Test
    @Category(UnitCategory.class)
    public void updatePasswordTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updatePassword(testUser.getId(), "new-password");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getPasswordHash(), "new-password");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateLoginTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updateLogin(testUser.getId(), "other-user");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getLogin(), "other-user");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateEmailTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updateEmail(testUser.getId(), "other-email@test.com");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getEmail(), "other-email@test.com");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateFirstNameTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updateFirstName(testUser.getId(),"other-first-name");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getFirstName(), "other-first-name");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateLastNameTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updateLastName(testUser.getId(), "other-last-name");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getLastName(), "other-last-name");
    }

    @Test
    @Category(UnitCategory.class)
    public void updateMiddleNameTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.updateMiddleName(testUser.getId(), "other-middle-name");
        final User user = userService.findById(testUser.getId());
        Assert.assertEquals(user.getId(), testUser.getId());
        Assert.assertEquals(user.getMiddleName(), "other-middle-name");
    }

    @Test
    @Category(UnitCategory.class)
    public void lockUserTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.lockUserByLogin(testUser.getLogin());
        final User user = userService.findById(testUser.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(UnitCategory.class)
    public void unlockUserTest() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.lockUserByLogin(testUser.getLogin());
        final User user = userService.findById(testUser.getId());
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(testUser.getLogin());
        Assert.assertFalse(user.getLocked());
    }
}
