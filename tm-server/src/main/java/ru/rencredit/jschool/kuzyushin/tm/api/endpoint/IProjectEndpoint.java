package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import java.util.List;

public interface IProjectEndpoint {

    @NotNull
    List<Project> findAllProjects(@Nullable Session session);

    void clearProjects(@Nullable Session session) throws Exception;

    void createProject(@Nullable Session session, @Nullable String name, @Nullable String description);

    @Nullable
    Project findProjectById(@Nullable Session session, @Nullable String id);

    @Nullable
    Project findProjectByName(@Nullable Session session, @Nullable String name);

    @Nullable
    Project removeProjectById(@Nullable Session session, @Nullable String id);

    @Nullable
    Project removeProjectByName(@Nullable Session session, @Nullable String name);

    @NotNull
    Project updateProjectById(@Nullable Session session, @Nullable String id,
                           @Nullable String name, @Nullable String description);
}
