package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IService;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected final IRepository<T> repository;

    protected AbstractService(@NotNull final IRepository<T> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public void load(final @Nullable List<T> ts) {
        if(ts == null) return;
        repository.load(ts);
    }

    @Override
    public void load(final @Nullable T[] ts) {
        if(ts == null) return;
        repository.load(ts);
    }
}
