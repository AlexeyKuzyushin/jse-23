package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IAdminUserEndpoint {

    @NotNull
    List<User> findAllUsers(@Nullable Session session);

    @Nullable
    User createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @Nullable
    User createUserWithRole(@Nullable Session session, @Nullable String login,
                            @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User lockUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    @Nullable
    User unlockUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    @Nullable
    User removeUserByLogin(@Nullable Session session, @Nullable String login) throws Exception;

    @Nullable
    User removeUserById(@Nullable Session session, @Nullable String id) throws Exception;
}
