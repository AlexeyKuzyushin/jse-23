package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyUserIdException;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(@NotNull String userId, @NotNull Session session) {
        session.setUserId(userId);
        records.add(session);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Session session) {
        if (!userId.equals(session.getUserId())) return;
        this.records.remove(session);
    }

    @Override
    public void clear(@NotNull String userId) {
        List<Session> sessions = findAllSessions(userId);
        this.records.removeAll(sessions);
    }

    @Override
    public @NotNull List<Session> findAllSessions(@NotNull String userId) {
        final @NotNull List<Session> result = new ArrayList<>();
        for (final @NotNull Session session: records) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @NotNull
    @Override
    public Session findSessionByUserId(@NotNull String userId) throws Exception{
        for (@NotNull final Session session : records) {
            if (userId.equals(session.getUserId())) return session;
        }
        throw new EmptyUserIdException();
    }

    @NotNull
    @Override
    public Session findSessionById(@NotNull String id) throws Exception{
        for (@NotNull final Session session : records) {
            if (id.equals(session.getId())) return session;
        }
        throw new EmptyIdException();
    }

    @NotNull
    @Override
    public Session removeSessionByUserId(@NotNull String userId) throws Exception {
        @NotNull final Session session = findSessionByUserId(userId);
        remove(userId, session);
        return session;
    }

    @NotNull
    @Override
    public boolean contains(@NotNull String id) throws Exception {
        @NotNull final Session session = findSessionById(id);
        return records.contains(session);
    }
}
