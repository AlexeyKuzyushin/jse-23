package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task updateOneById(@Nullable String userId, @Nullable String id,
                       @Nullable String name, @Nullable String description);

    @NotNull
    Task updateOneByIndex(@Nullable String userId, @Nullable Integer index,
                          @Nullable String name, @Nullable String description);
}
