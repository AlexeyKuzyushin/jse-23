package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void add(@Nullable String userId, @Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> findALl(@Nullable String userId);

    void clear(@Nullable String userId);

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project updateOneById(@Nullable String userId, @Nullable String id,
                          @Nullable String name, @Nullable String description);
    @Nullable
    Project updateOneByIndex(@Nullable String userId, @Nullable Integer index,
                             @Nullable String name, @Nullable String description);
}
