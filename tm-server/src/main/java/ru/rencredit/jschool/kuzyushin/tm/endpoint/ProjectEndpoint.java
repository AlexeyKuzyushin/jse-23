package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findALl(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id) {
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name) {
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id) {
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable String name) {
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable String id,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        return serviceLocator.getProjectService().updateOneById(session.getUserId(), id, name, description);
    }
}
