package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAllTasks(@NotNull String userId);

    @Nullable
    Task findTaskById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findTaskByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeTaskById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeTaskByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeTaskByIndex(@NotNull String userId, @NotNull Integer index);
}
