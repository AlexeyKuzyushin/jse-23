package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAllProjects(@NotNull String userId);

    @Nullable
    Project findProjectById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findProjectByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeProjectById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeProjectByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeProjectByIndex(@NotNull String userId, @NotNull Integer index);
}
