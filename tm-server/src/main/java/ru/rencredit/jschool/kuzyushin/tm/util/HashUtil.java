package ru.rencredit.jschool.kuzyushin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull
    String SECRET = "145326732";

    @NotNull
    Integer ITERATION = 12876;

    @Nullable
    static String salt(final @NotNull String value) {
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@NotNull final String value) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
