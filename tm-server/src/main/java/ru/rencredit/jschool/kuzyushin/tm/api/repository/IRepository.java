package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository <E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add (@NotNull E e);

    @NotNull
    E remove (@NotNull E e);

    void add (@NotNull List<E> es);

    void add (@NotNull E[] es);

    void load (@NotNull List<E> es);

    void load (@NotNull E[] es);

    void clear();
}
