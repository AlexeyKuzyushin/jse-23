package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void add(final @NotNull String userId,
                    final @NotNull Project project) {
        project.setUserId(userId);
        records.add(project);
    }

    @Override
    public void remove(final @NotNull String userId,
                       final @NotNull Project project) {
        if (!userId.equals(project.getUserId())) return;
        this.records.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAllProjects(final @NotNull String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final @NotNull Project project: records) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final @NotNull List<Project> projects = findAllProjects(userId);
        this.records.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findProjectById(final @NotNull String userId,
                                   final @NotNull String id) {
        final @NotNull List<Project> projects = findAllProjects(userId);
        for (final @NotNull Project project: projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findProjectByName(final @NotNull String userId,
                                     final @NotNull String name) {
        final @NotNull List<Project> projects = findAllProjects(userId);
        for (final @NotNull Project project: projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findProjectByIndex(final @NotNull String userId,
                                      final @NotNull Integer index) {
        final @NotNull List<Project> projects = findAllProjects(userId);
        if (!projects.isEmpty()) return projects.get(index);
        return null;
    }

    @Nullable
    @Override
    public Project removeProjectByName(final @NotNull String userId,
                                       final @NotNull String name) {
        final @Nullable Project project = findProjectByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeProjectByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final @Nullable Project project = findProjectByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeProjectById(final @NotNull String userId,
                                     final @NotNull String id) {
        final @Nullable Project project = findProjectById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }
}
