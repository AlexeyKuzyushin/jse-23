package ru.rencredit.jschool.kuzyushin.tm.test.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.Bootstrap;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.User;
import ru.rencredit.jschool.kuzyushin.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    private final User testUser = new User();

    @Before
    public void initData() throws Exception_Exception {
        testUser.setLogin("userOne");
        testUser.setEmail("testUser@test.com");
        testUser.setPasswordHash("password");
        testUser.setFirstName("first-name");
        testUser.setLastName("last-name");
        testUser.setMiddleName("middle-name");
        serviceLocator.getAdminUserEndpoint().createUserWithEmail(testUser.getLogin(), testUser.getPasswordHash(),
                testUser.getEmail());
        final Session session = serviceLocator.getSessionEndpoint().openSession(testUser.getLogin(),
                testUser.getPasswordHash());
        serviceLocator.getUserEndpoint().updateUserFirstName(session, testUser.getFirstName());
        serviceLocator.getUserEndpoint().updateUserLastName(session, testUser.getLastName());
        serviceLocator.getUserEndpoint().updateUserMiddleName(session, testUser.getMiddleName());
        serviceLocator.getSessionService().setCurrentSession(session);
    }

    @After
    public void clearData() throws Exception_Exception {
        final Session testUserSession = serviceLocator.getSessionService().getCurrentSession();
        final Session adminSession = serviceLocator.getSessionEndpoint().openSession("admin","admin");
        serviceLocator.getAdminUserEndpoint().removeUserByLogin(adminSession, testUser.getLogin());
        serviceLocator.getSessionEndpoint().closeSession(testUserSession);
        serviceLocator.getSessionEndpoint().closeSession(adminSession);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().viewUserProfile(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateLoginTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserLogin(session, "newLogin");
        Assert.assertNotNull(user);
        testUser.setLogin("newLogin");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateEmailTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserEmail(session, "newEmail@test.com");
        Assert.assertNotNull(user);
        testUser.setEmail("newEmail@test.com");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateFirstNameTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserFirstName(session, "newFirst-name");
        Assert.assertNotNull(user);
        Assert.assertNotEquals(user.getFirstName(), testUser.getFirstName());
        testUser.setFirstName("newFirst-name");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
        Assert.assertEquals(user.getFirstName(), testUser.getFirstName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateLastNameTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserLastName(session, "newLast-name");
        Assert.assertNotNull(user);
        Assert.assertNotEquals(user.getLastName(), testUser.getLastName());
        testUser.setLastName("newLast-name");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
        Assert.assertEquals(user.getLastName(), testUser.getLastName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updateMiddleNameTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserMiddleName(session, "newMiddle-name");
        Assert.assertNotNull(user);
        Assert.assertNotEquals(user.getMiddleName(), testUser.getMiddleName());
        testUser.setMiddleName("newMiddle-name");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
        Assert.assertEquals(user.getMiddleName(), testUser.getMiddleName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void updatePasswordTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().updateUserPassword(session, "newPassword");
        Assert.assertNotNull(user);
        Assert.assertNotEquals(user.getPasswordHash(), testUser.getPasswordHash());
        testUser.setPasswordHash("newPassword");
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), testUser.getPasswordHash());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void viewUserTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getUserEndpoint().viewUserProfile(session);
        Assert.assertEquals(user.getLogin(), testUser.getLogin());
        Assert.assertEquals(user.getEmail(), testUser.getEmail());
        Assert.assertEquals(user.getFirstName(), testUser.getFirstName());
        Assert.assertEquals(user.getMiddleName(), testUser.getMiddleName());
        Assert.assertEquals(user.getLastName(), testUser.getLastName());
    }
}
