package ru.rencredit.jschool.kuzyushin.tm.test.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.Bootstrap;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.User;
import ru.rencredit.jschool.kuzyushin.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class AdminUserEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    private final User testUser = new User();

    @Before
    public void initData() throws Exception_Exception {
        testUser.setLogin("userOne");
        testUser.setEmail("testUser@test.com");
        testUser.setPasswordHash("password");
        final Session session = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        serviceLocator.getSessionService().setCurrentSession(session);
    }

    @After
    public void clearData() {
        serviceLocator.getSessionEndpoint().closeSession(serviceLocator.getSessionService().getCurrentSession());
        serviceLocator.getSessionService().clearCurrentSession();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithEmail(testUser.getLogin(), testUser.getPasswordHash(),
                testUser.getEmail());
        Assert.assertEquals(3, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserByLogin(session, testUser.getLogin());
        Assert.assertEquals(2, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithEmail(testUser.getLogin(), testUser.getPasswordHash(),
                testUser.getEmail());
        Assert.assertEquals(3, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
        Assert.assertEquals(2, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserWithEmailTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        Assert.assertEquals(2, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithEmail(testUser.getLogin(), testUser.getPasswordHash(),
                testUser.getEmail());
        Assert.assertEquals(3, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void createUserWithRoleTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        Assert.assertEquals(2, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithRole(session, testUser.getLogin(),
                testUser.getPasswordHash(), Role.USER);
        Assert.assertEquals(3, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        Assert.assertEquals(user.getRole(), Role.USER);
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllUsersTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        Assert.assertEquals(2, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithRole(session, testUser.getLogin(),
                testUser.getPasswordHash(), Role.USER);
        Assert.assertEquals(3, serviceLocator.getAdminUserEndpoint().findAllUsers(session).size());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithRole(session, testUser.getLogin(),
                testUser.getPasswordHash(), Role.USER);
        final User lockedUser = serviceLocator.getAdminUserEndpoint().lockUserByLogin(session, user.getLogin());
        Assert.assertTrue(lockedUser.isLocked());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final User user = serviceLocator.getAdminUserEndpoint().createUserWithRole(session, testUser.getLogin(),
                testUser.getPasswordHash(), Role.USER);
        final User lockedUser = serviceLocator.getAdminUserEndpoint().lockUserByLogin(session, user.getLogin());
        Assert.assertTrue(lockedUser.isLocked());
        final User unlockedUser = serviceLocator.getAdminUserEndpoint().unlockUserByLogin(session, user.getLogin());
        Assert.assertFalse(unlockedUser.isLocked());
        serviceLocator.getAdminUserEndpoint().removeUserById(session, user.getId());
    }
}
