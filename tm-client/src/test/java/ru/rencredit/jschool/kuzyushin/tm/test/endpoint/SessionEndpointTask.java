package ru.rencredit.jschool.kuzyushin.tm.test.endpoint;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.Bootstrap;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.marker.IntegrationCategory;

public class SessionEndpointTask {

    private final IServiceLocator serviceLocator = new Bootstrap();

    @Test
    @Category(IntegrationCategory.class)
    public void openSessionTest() throws Exception_Exception {
        final Session adminSession = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        Assert.assertNotNull(adminSession);
        serviceLocator.getAdminUserEndpoint().createUserWithEmail("testUser", "testUserPassword",
                "testUser@test.com");
        final Session userSession = serviceLocator.getSessionEndpoint().openSession("testUser",
                "testUserPassword");
        Assert.assertNotNull(userSession);
        Assert.assertNotNull(serviceLocator.getSessionEndpoint().closeSession(adminSession));
        Assert.assertNotNull(serviceLocator.getSessionEndpoint().closeSession(userSession));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void closeAllSessionTest() throws Exception_Exception {
        final Session adminSession1 = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        Assert.assertNotNull(adminSession1);
        Assert.assertNotNull(serviceLocator.getSessionEndpoint().closeAllUserSession(adminSession1));
    }
}
