package ru.rencredit.jschool.kuzyushin.tm.test.endpoint;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.Bootstrap;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.marker.IntegrationCategory;

public class DataEndpointTest {

    private final IServiceLocator serviceLocator = new Bootstrap();

    @Before
    public void initData() throws Exception_Exception {
        final Session session = serviceLocator.getSessionEndpoint().openSession("admin", "admin");
        serviceLocator.getTaskEndpoint().createTask(session, "123123", "testTask");
        serviceLocator.getProjectEndpoint().createProject(session, "123123", "testProject");
        serviceLocator.getSessionService().setCurrentSession(session);
    }

    @After
    public void clearData() throws Exception_Exception {
        serviceLocator.getProjectEndpoint().clearProjects(serviceLocator.getSessionService().getCurrentSession());
        serviceLocator.getTaskEndpoint().clearTasks(serviceLocator.getSessionService().getCurrentSession());
        serviceLocator.getSessionEndpoint().closeSession(serviceLocator.getSessionService().getCurrentSession());
        serviceLocator.getSessionService().clearCurrentSession();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveLoadClearDataBinary() throws Exception_Exception {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        serviceLocator.getDataEndpoint().saveDataBinary(session);
        serviceLocator.getProjectEndpoint().clearProjects(session);
        serviceLocator.getTaskEndpoint().clearTasks(session);
        Assert.assertEquals(0, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(0, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().loadDataBinary(session);
        Assert.assertEquals(1, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(1, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().clearDataBinary(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveLoadClearDataJsonTest() throws Exception_Exception {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        serviceLocator.getDataEndpoint().saveDataJson(session);
        serviceLocator.getProjectEndpoint().clearProjects(session);
        serviceLocator.getTaskEndpoint().clearTasks(session);
        Assert.assertEquals(0, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(0, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().loadDataJson(session);
        Assert.assertEquals(1, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(1, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().clearDataJson(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveLoadClearDataXmlTest() throws Exception_Exception {
        final Session session = serviceLocator.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        serviceLocator.getDataEndpoint().saveDataXml(session);
        serviceLocator.getProjectEndpoint().clearProjects(session);
        serviceLocator.getTaskEndpoint().clearTasks(session);
        Assert.assertEquals(0, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(0, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().loadDataXml(session);
        Assert.assertEquals(1, serviceLocator.getTaskEndpoint().findAllTasks(session).size());
        Assert.assertEquals(1, serviceLocator.getProjectEndpoint().findAllProjects(session).size());
        serviceLocator.getDataEndpoint().clearDataXml(session);
    }
}
