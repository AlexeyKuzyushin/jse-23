package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("[ENTER LOGIN:]");
            @Nullable final String login = TerminalUtil.nextLine();
            serviceLocator.getAdminUserEndpoint().unlockUserByLogin(session, login);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
