package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-22T15:37:00.972+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/viewUserProfileRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/viewUserProfileResponse")
    @RequestWrapper(localName = "viewUserProfile", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.ViewUserProfile")
    @ResponseWrapper(localName = "viewUserProfileResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.ViewUserProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User viewUserProfile(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserPasswordRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserPasswordResponse")
    @RequestWrapper(localName = "updateUserPassword", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserPassword")
    @ResponseWrapper(localName = "updateUserPasswordResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserPassword(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserLastNameRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserLastNameResponse")
    @RequestWrapper(localName = "updateUserLastName", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserLastName")
    @ResponseWrapper(localName = "updateUserLastNameResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserLastNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserLastName(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserMiddleNameRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserMiddleNameResponse")
    @RequestWrapper(localName = "updateUserMiddleName", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserMiddleName")
    @ResponseWrapper(localName = "updateUserMiddleNameResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserMiddleNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserMiddleName(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserLoginRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserLoginResponse")
    @RequestWrapper(localName = "updateUserLogin", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserLogin")
    @ResponseWrapper(localName = "updateUserLoginResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserLogin(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserEmailRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserEmailResponse")
    @RequestWrapper(localName = "updateUserEmail", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserEmail")
    @ResponseWrapper(localName = "updateUserEmailResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserEmail(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserFirstNameRequest", output = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/UserEndpoint/updateUserFirstNameResponse")
    @RequestWrapper(localName = "updateUserFirstName", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserFirstName")
    @ResponseWrapper(localName = "updateUserFirstNameResponse", targetNamespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.UpdateUserFirstNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.User updateUserFirstName(
        @WebParam(name = "session", targetNamespace = "")
        ru.rencredit.jschool.kuzyushin.tm.endpoint.Session session,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName
    );
}
