package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user login";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LOGIN]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER LOGIN:");
            @Nullable final String login = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserLogin(session, login);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
