package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Project;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            @NotNull final List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjects(session);
            int index = 1;
            for (Project project: projects) {
                System.out.println(index + ". " + project.getName());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
