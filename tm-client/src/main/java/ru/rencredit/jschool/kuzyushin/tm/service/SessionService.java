package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public class SessionService implements ISessionService {

    @Nullable
    private Session currentSession;

    @Nullable
    @Override
    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(final Session currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }
}
