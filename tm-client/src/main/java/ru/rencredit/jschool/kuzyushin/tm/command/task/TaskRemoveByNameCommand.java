package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        if (serviceLocator != null) {
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().removeTaskByName(session, name);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
