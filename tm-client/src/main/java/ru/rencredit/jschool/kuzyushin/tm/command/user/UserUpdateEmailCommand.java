package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateEmailCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-email";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user e-mail";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER E-MAIL]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER ID:");
            @Nullable final String id = TerminalUtil.nextLine();
            System.out.println("ENTER EMAIL:");
            @Nullable final String email = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserEmail(session, email);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
